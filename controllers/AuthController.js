const io = require('../io');
const requestJson = require ('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechupatxi10ed/collections/";
const mLabAPIKey = "apiKey="+ process.env.MLAB_API_KEY;


const crypt = require('../crypt');


function loginUserV1(req, res) {
 console.log("POST /javiBank/v1/login");
 console.log("email:"+req.body.email);
 console.log("password:"+req.body.password);


 var query = 'q={"email": "' + req.body.email + '"}';
 console.log("query es " + query);

 httpClient = requestJson.createClient(baseMLabURL);
 httpClient.get("user?" + query + "&" + mLabAPIKey,

  function(err, resMLab, body){
    console.log (body);
    if(body.length > 0){
      console.log("Email encontrado");
      console.log(body[0].password);
      if (crypt.checkPassword(req.body.password, body[0].password)){
        console.log("Password coincide");
        var id = body[0].id;
        query = 'q={"id":' + id + '}';
        var putBody = '{"$set":{"logged":true}}';
        httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
          function(err2, resMLab2, body2){
            if (!err2){
              console.log("Cliente logado");
              res.send({"mensaje" : "Login correcto", "idUsuario" : id});
            }
          }
        )
      }else{
        console.log("Password no coincide");
        res.status(401);
        res.send({"mensaje" : "Login incorrecto"});
      }
    }else{
      console.log("Email no encontrado");
      res.status(401);
      res.send({"mensaje" : "Login incorrecto"});
    }
  }
)
}

function logoutUserV1(req, res) {
 console.log("POST /javiBank/v1/logout/:id");

 var query = 'q={"id": ' + req.params.id + '}';
 console.log("query es " + query);

 httpClient = requestJson.createClient(baseMLabURL);
 httpClient.get("user?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {
     if (body.length == 0) {
       var response = {
         "mensaje" : "Logout incorrecto, usuario no encontrado"
       }
       res.send(response);
     } else {
       console.log("Got a user with that id, logging out");
       query = 'q={"id" : ' + body[0].id +'}';
       console.log("Query for put is " + query);
       var putBody = '{"$unset":{"logged":""}}'
       httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
         function(errPUT, resMLabPUT, bodyPUT) {
           console.log("PUT done");
           var response = {
             "msg" : "Usuario deslogado",
             "idUsuario" : body[0].id
           }
           res.send(response);
         }
       )
     }
   }
 );
}



module.exports.loginUserV1 = loginUserV1;
module.exports.logoutUserV1 = logoutUserV1;
